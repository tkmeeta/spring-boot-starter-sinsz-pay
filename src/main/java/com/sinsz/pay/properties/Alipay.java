package com.sinsz.pay.properties;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;

/**
 * 支付宝支付配置
 * @author chenjianbo
 */
@Validated
@ConfigurationProperties("sinsz.pay.alipay")
public class Alipay {

    /**
     * 支付宝接入应用ID
     */
    @NotEmpty
    private String appid;

    /**
     * 公共密钥文件地址
     */
    @NotEmpty
    private String publicKey;

    /**
     * 私有密钥文件地址
     */
    @NotEmpty
    private String privateKey;

    /**
     * 支付结果回调
     */
    @NotEmpty
    private String payNotify;

    /**
     * 退款结果回调
     */
    private String refundNotify = "";

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getPayNotify() {
        return payNotify;
    }

    public void setPayNotify(String payNotify) {
        this.payNotify = payNotify;
    }

    public String getRefundNotify() {
        return refundNotify;
    }

    public void setRefundNotify(String refundNotify) {
        this.refundNotify = refundNotify;
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace(System.out);
        }
        return super.toString();
    }

}
