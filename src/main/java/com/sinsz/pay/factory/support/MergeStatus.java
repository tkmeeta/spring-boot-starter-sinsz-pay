package com.sinsz.pay.factory.support;

import org.apache.commons.lang.StringUtils;

/**
 * 合并交易状态
 * @author chenjianbo
 */
public enum MergeStatus {

    QUERY_PAY_SUCCESS(1, "SUCCESS", "支付成功"),
    QUERY_PAY_NOTPAY(1, "NOTPAY", "等待支付"),
    QUERY_PAY_FAIL(1, "PAYERROR", "支付失败"),
    QUERY_PAY_CLOSED(1, "CLOSED", "交易关闭(未支付,转入退款,撤销)"),
    QUERY_PAY_USERPAYING(1, "USERPAYING", "正在支付"),
    QUERY_PAY_FINISHED(1, "FINISHED", "交易结束(不可退款)"),
    QUERY_PAY_UNKNOWN(1, "UNKNOWN", "其他状态"),

    QUERY_REFUND_SUCCESS(2, "SUCCESS", "退款成功"),
    QUERY_REFUND_CLOSE(2, "REFUNDCLOSE", "退款关闭"),
    QUERY_REFUND_PROCESSING(2, "PROCESSING", "退款处理中"),
    QUERY_REFUND_CHANGE(2, "CHANGE", "退款异常"),
    QUERY_REFUND_UNKNOWN(2, "UNKNOWN", "其他状态"),

    QUERY_TRANSFER_SUCCESS(3, "SUCCESS", "转账成功"),
    QUERY_TRANSFER_FAILED(3, "FAILED", "转账失败"),
    QUERY_TRANSFER_PROCESSING(3, "PROCESSING", "转账处理中"),
    QUERY_TRANSFER_UNKNOWN(3, "UNKNOWN", "状态未知"),

    ;

    private int type;


    private String status;


    private String desc;

    MergeStatus(int type, String status, String desc) {
        this.type = type;
        this.status = status;
        this.desc = desc;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * 微信支付状态合并
     * <p>
     *     订单查询到的状态
     * </p>
     * @param name
     * @return
     */
    public static MergeStatus wxPayStatus(String name) {
        if (StringUtils.isEmpty(name)) {
            return QUERY_PAY_UNKNOWN;
        }
        switch (name) {
            case "SUCCESS":
                return QUERY_PAY_SUCCESS;
            case "NOTPAY":
                return QUERY_PAY_NOTPAY;
            case "PAYERROR":
                return QUERY_PAY_FAIL;
            case "CLOSED":
            case "REFUND":
            case "REVOKED":
                return QUERY_PAY_CLOSED;
            case "USERPAYING":
                return QUERY_PAY_USERPAYING;
            default:
                return QUERY_PAY_UNKNOWN;
        }
    }

    /**
     * 支付宝支付状态合并
     * @param name
     * @return
     */
    public static MergeStatus aliPayStatus(String name) {
        if (StringUtils.isEmpty(name)) {
            return QUERY_PAY_UNKNOWN;
        }
        switch (name) {
            case "TRADE_SUCCESS":
                return QUERY_PAY_SUCCESS;
            case "WAIT_BUYER_PAY":
                return QUERY_PAY_NOTPAY;
            case "TRADE_CLOSED":
                return QUERY_PAY_CLOSED;
            case "TRADE_FINISHED":
                return QUERY_PAY_FINISHED;
            default:
                return QUERY_PAY_UNKNOWN;
        }
    }

    /**
     * 微信退款状态合并
     * @param name
     * @return
     */
    public static MergeStatus wxRefundStatus(String name) {
        if (StringUtils.isEmpty(name)) {
            return QUERY_REFUND_UNKNOWN;
        }
        switch (name) {
            case "SUCCESS":
                return QUERY_REFUND_SUCCESS;
            case "REFUNDCLOSE":
                return QUERY_REFUND_CLOSE;
            case "PROCESSING":
                return QUERY_REFUND_PROCESSING;
            case "CHANGE":
                return QUERY_REFUND_CHANGE;
            default:
                return QUERY_REFUND_UNKNOWN;
        }
    }

    /**
     * 支付宝退款状态合并
     * @return
     */
    public static MergeStatus aliRefundStatus() {
        return QUERY_REFUND_SUCCESS;
    }

    /**
     * 微信转账状态合并
     * @param name
     * @return
     */
    public static MergeStatus wxTransferStatus(String name) {
        if (StringUtils.isEmpty(name)) {
            return QUERY_TRANSFER_UNKNOWN;
        }
        switch (name) {
            case "SUCCESS":
                return QUERY_TRANSFER_SUCCESS;
            case "FAILED":
                return QUERY_TRANSFER_FAILED;
            case "PROCESSING":
                return QUERY_TRANSFER_PROCESSING;
            default:
                return QUERY_TRANSFER_UNKNOWN;
        }
    }

    /**
     * 支付宝转账状态合并
     * @param name
     * @return
     */
    public static MergeStatus aliTransferStatus(String name) {
        if (StringUtils.isEmpty(name)) {
            return QUERY_TRANSFER_UNKNOWN;
        }
        switch (name) {
            case "SUCCESS":
                return QUERY_TRANSFER_SUCCESS;
            case "FAIL":
            case "REFUND":
                return QUERY_TRANSFER_FAILED;
            case "INIT":
            case "DEALING":
                return QUERY_TRANSFER_PROCESSING;
            default:
                return QUERY_TRANSFER_UNKNOWN;
        }
    }


}
