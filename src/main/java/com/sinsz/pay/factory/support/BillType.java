package com.sinsz.pay.factory.support;

/**
 * 账单类型
 * @author chenjianbo
 */
public enum BillType {

    /**
     * 所有订单信息
     * <p>
     *     仅微信使用
     * </p>
     */
    ALL,

    /**
     * 成功支付的订单
     * <p>
     *     仅微信使用
     * </p>
     */
    SUCCESS,

    /**
     * 退款订单
     * <p>
     *     仅微信使用
     * </p>
     */
    REFUND,

    /**
     * 充值退款订单
     * <p>
     *     仅微信使用
     * </p>
     */
    RECHARGE_REFUND,

    /**
     * trade指商户基于支付宝交易收单的业务账单；
     * <p>
     *     仅支付宝使用
     * </p>
     */
    TRADE,

    /**
     * signcustomer是指基于商户支付宝余额收入及支出等资金变动的帐务账单；
     * <p>
     *     仅支付宝使用
     * </p>
     */
    SIGNCUSTOMER

}
